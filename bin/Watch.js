const path = require('path');
var webpack = require('sgmf-scripts').webpack;
const chokidar = require('chokidar');
const cwd = process.cwd();

const webpackConfig = require('sgmf-scripts/lib/utils/webpackConfig');


(function watch() {
    const packageFile = require(path.join(cwd, './package.json'));
    const jsConfig = webpackConfig(packageFile, cwd, 'js');
    const scssConfig = webpackConfig(packageFile, cwd, 'scss');
    const config = [];
    if (typeof jsConfig !== Error) {
        config.push(jsConfig);
    }
    if (typeof scssConfig !== Error) {
        config.push(scssConfig);
    }
    const compiler = webpack(config);

    compiler.watch({
        aggregateTimeout: 300,
        poll: undefined
    }, (err) => {
        if (err) {
            console.log(err);
        }
    });

    if (true) {
        const pathToCut = path.join(cwd, 'cartridges', 'app_best_store', 'cartridge', 'static', 'default');
        const watcher = chokidar.watch(pathToCut, {
            persistent: true,
            ignoreInitial: true,
            followSymlinks: false,
            awaitWriteFinish: {
                stabilityThreshold: 300,
                pollInterval: 100
            }
        });

        watcher.on('change', filename => {
            console.log('\x1b[32m%s\x1b[0m', `Detected change in file: ${filename.replace(pathToCut, '')}`);
            //uploadFiles([filename]);
        });

        watcher.on('add', filename => {
            console.log('\x1b[34m%s\x1b[0m', `Detected added file: ${filename}`);
            //uploadFiles([filename]);
        });

        watcher.on('unlink', filename => {
            console.log('\x1b[31m', `Detected deleted file: ${filename}`);
            //deleteFiles([filename]);
        });
    }
})();
