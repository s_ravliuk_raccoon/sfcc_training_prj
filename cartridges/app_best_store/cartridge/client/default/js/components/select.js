'use strict';

require('select2');

var init = function () {
    if ($('.custom-select').length === 0) {
        return;
    }

    var options = {
        minimumResultsForSearch: Infinity,
        width: '100%',
        dropdownCssClass: 'dropdown-opened'
    };

    if ($('.custom-select').attr('class').indexOf('text-right') !== -1) {
        options.selectionCssClass = 'text-right';
    }

    $('.custom-select').select2(options);
};


module.exports = function () {
    init();
};
